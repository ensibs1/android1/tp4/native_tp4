#include <jni.h>
#include <string>
#include <android/log.h>
#include <unistd.h>
#include <setjmp.h>
#include <signal.h>
#include <assert.h>
#include <asm/unistd.h>

static const char *tag = "telescope";

extern "C" JNIEXPORT jstring JNICALL
Java_com_andsec_tp3_native_1tp4_MainActivity_eval(JNIEnv* env, jobject /* this */, jstring cmd) {
    int ret;
    char output_bytes[4096];
    FILE *f;
    const char *cmd_bytes;
    jstring output;

    cmd_bytes = env->GetStringUTFChars(cmd, NULL);
    if (!cmd_bytes) {
        __android_log_print(ANDROID_LOG_DEBUG, tag, "failed to get bytes of cmd string\n");
        goto failed_cmd_bytes;
    }

    f = popen(cmd_bytes, "r");
    if (!f) {
        __android_log_print(ANDROID_LOG_DEBUG, tag, "failed to create process\n");
        goto failed_popen;
    }

    memset(output_bytes, 0, sizeof(output_bytes));
    ret = fread(output_bytes, sizeof(output_bytes)-1, 1, f);
    if (ferror(f)) {
        __android_log_print(ANDROID_LOG_DEBUG, tag, "failed to read command output\n");
        goto failed_fread;
    }

    output_bytes[sizeof(output_bytes)-1] = 0;

    output = env->NewStringUTF(output_bytes);
    if (!output) {
        __android_log_print(ANDROID_LOG_DEBUG, tag, "failed to create output string\n");
        goto failed_fread;
    }

    pclose(f);
    env->ReleaseStringUTFChars(cmd, cmd_bytes);

    return output;

    failed_fread:
    pclose(f);
    failed_popen:
    env->ReleaseStringUTFChars(cmd, cmd_bytes);
    failed_cmd_bytes:
    return env->NewStringUTF("<error>");
}