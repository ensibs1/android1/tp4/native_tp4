package com.andsec.tp3.native_tp4;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.andsec.tp3.native_tp4.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    // Used to load the 'native_tp4' library on application startup.
    static {
        System.loadLibrary("native_tp4");
    }

    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void sendCommand(View view) {
        EditText text = (EditText)findViewById(R.id.editCommand);
        String value = text.getText().toString();
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        // Example of a call to a native method
        TextView tv = binding.sampleText;
        tv.setMovementMethod(new ScrollingMovementMethod());
        tv.setText(eval(value));
    }
    /**
     * A native method that is implemented by the 'native_tp4' native library,
     * which is packaged with this application.
     */
    private native String eval(String cmd);
    //public native String stringFromJNI();
}